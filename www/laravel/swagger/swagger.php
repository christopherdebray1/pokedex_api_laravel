<?php

use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="Pokedex api", version="1"),
 * @OA\Server(
 *    url="http://pokedex_api.local/",
 *    description="A pokedex api",
 * ),
 * 
 * @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of the ressource",
 *      required=true,
 *      @OA\Schema(type="integer"),
 * ),
 * 
 * @OA\Response(
 *      response="NotFound",
 *      description="This ressource doesn't exist",
 *      @OA\JsonContent(
 *          @OA\Property(
 *              property="success",
 *              type="boolean",
 *              example=false
 *          ),
 *      ),
 * ),
 * 
 */