<?php

use App\Http\Controllers\Pokemon\PokemonController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(UserController::class)->group(function () {
    Route::prefix('users')->group(function () {
        Route::post('/register', 'register')->name('register');
        Route::post('/login', 'login')->name('login');

        Route::middleware('auth:sanctum')-> group(function () {
            Route::post('/logout', 'logout')->name('logout');
        });
    });
});

Route::controller(PokemonController::class)->group(function () {
    Route::prefix('pokemons')->group(function () {
        Route::middleware('auth:sanctum')->group(function () {
            Route::get('', 'index')->name('pokemon_all');
            Route::get('/{pokemon}', 'getOne')->name('pokemon_get');
            Route::post('', 'create')->name('pokemon_create');
            Route::post('/users/team/{user}', 'createPokemonTeam')->name('pokemon_user_team_create');
            Route::get('/users/team/{user}', 'getPokemonTeam')->name('pokemon_user_team_get');
        });
    });
});