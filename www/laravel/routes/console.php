<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('db:create', function () {
    $databaseName = env('DB_DATABASE');
    config(['database.connections.mysql.database' => null]);

    DB::statement("CREATE DATABASE IF NOT EXISTS $databaseName");
    config(['database.connections.mysql.database' => $databaseName]);

    $this->info("Database $databaseName created successfully.");
})->purpose('Create the database from the environement configuration');

Artisan::command('db:drop', function () {
    $databaseName = env('DB_DATABASE');
    DB::statement("DROP DATABASE IF EXISTS $databaseName");
})->purpose('Drop the database from the environement configuration if it exist');