<?php

namespace Database\Seeders\Pokemon;

use Illuminate\Database\Seeder;
use App\Models\Pokemon\Pokemon;

class PokemonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Pokemon::factory(3)
            ->hasUsers(1)
            ->create();
    }
}
