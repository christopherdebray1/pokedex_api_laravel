<?php

namespace Database\Seeders\User;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(5)->create();
        User::factory()->create([
            'email' => 'christopherdebray@gmail.com',
            'name' => 'test',
            'password' => 'test',
            'profile_picture' => 'no_picture.jpeg',
        ]);
    }
}
