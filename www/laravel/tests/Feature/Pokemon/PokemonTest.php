<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\AbstractTest;

class PokemonTest extends AbstractTest
{
    use RefreshDatabase;
    protected $seed = true;

    /**
     * Test multiple cases of pokemon get with existing or not pokemon and with or without authorization
     * 
     * @return void
     */
    public function getOneTest(): void
    {

    }

    /**
     * Test multiple cases of pokemon creation with both valid and invalid data or authorization
     * 
     * @param array $payload
     * @param int $expectedResponseCode
     * @param array $expectedFieldErrors
     * @param bool $isAuthenticated
     * 
     * @return void
     * 
     * @dataProvider provideCreateData
     */
    public function testCreate(array $payload, int $expectedResponseCode, array $expectedFieldErrors, bool $isAuthenticated = true): void
    {
        if ($isAuthenticated) {
            $this->authValidUser();
        }

        $response = $this->post(route('pokemon_create'), $payload);

        $response->assertStatus($expectedResponseCode);
        if ($expectedFieldErrors) {
            $response->assertInvalid($expectedFieldErrors);
        }
    }

    /**
     * Provide data for the testCreate method
     * 
     * @return array
     */
    static public function provideCreateData(): array
    {
        return [
            'Valid data' => [
                [
                    'name' => 'Charizard',
                    'pokedex_id' => '100',
                    'picture_url' => fake()->url(),
                ],
                Response::HTTP_CREATED,
                []
            ],
            'Not authenticated' => [
                [
                    'name' => fake()->name(),
                    'pokedex_id' => fake()->numberBetween(1, 999),
                    'picture_url' => fake()->url(),
                ],
                Response::HTTP_UNAUTHORIZED,
                [],
                false
            ],
            'No name in payload' => [
                [
                    'name' => '',
                    'pokedex_id' => fake()->numberBetween(1, 999),
                    'picture_url' => fake()->url(),
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                [],
            ],
            'No pokedex_id in payload' => [
                [
                    'name' => fake()->name(),
                    'pokedex_id' => '',
                    'picture_url' => fake()->url(),
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                [],
            ],
            'No picture_url in payload' => [
                [
                    'name' => fake()->name(),
                    'pokedex_id' => fake()->numberBetween(1,999),
                    'picture_url' => '',
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                [],
            ],
        ];
    }
}
