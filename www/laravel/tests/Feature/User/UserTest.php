<?php

namespace Tests\Feature\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\AbstractTest;

class UserTest extends AbstractTest
{
    use RefreshDatabase;

    /**
     * Test multiple cases of register with both valid and invalid data or authorization
     * 
     * @param array $payload
     * @param int $expectedResponseCode
     * @param array $expectedFieldErrors
     * 
     * @return void
     * 
     * @dataProvider registerProvider
     */
    public function testRegister(array $payload, int $expectedResponseCode, array $expectedFieldErrors): void
    {
        $response = $this->post(route('register'), $payload);

        $response->assertStatus($expectedResponseCode);
        if ($expectedFieldErrors) {
            $response->assertInvalid($expectedFieldErrors);
        }
    }

    /**
     * Test multiple cases of login with both valid and invalid data or authorization
     * 
     * @param array $payload
     * @param int $expectedResponseCode
     * @param array $expectedFieldErrors
     * 
     * @return void
     * 
     * @dataProvider loginProvider
     */
    public function testLogin(array $payload, int $expectedResponseCode, array $expectedFieldErrors): void
    {
        $response = $this->post(route('login'), $payload);

        $response->assertStatus($expectedResponseCode);
        if ($expectedFieldErrors) {
            $response->assertInvalid($expectedFieldErrors);
        }
    }

    /**
     * Provide data for the testRegister method
     * 
     * @return array
     */
    static public function registerProvider(): array
    {
        return [
            'Valid data with default picture' => [
                [
                    "email" => "createVdefaultPic@gmail.com",
                    "name" => "test",
                    "password" => "test",
                    "default_picture" => "1",
                ],
                Response::HTTP_CREATED,
                []
            ],
            'Invalid data no password' => [
                [
                    "email" => "createIVnoPsw@gmail.com",
                    "name" => "test",
                    "password" => "",
                    "default_picture" => "1",
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ["password"]
            ],
            'Invalid data no email' => [
                [
                    "email" => "",
                    "name" => "test",
                    "password" => "test",
                    "default_picture" => "1",
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ["email"]
            ],
            'Invalid data no name' => [
                [
                    "email" => "createIVnoName@gmail.com",
                    "name" => "",
                    "password" => "test",
                    "default_picture" => "1",
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ["name"]
            ],
            'Invalid data no picture' => [
                [
                    "email" => "createIVnoPic@gmail.com",
                    "name" => "test",
                    "password" => "test",
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ["profile_picture", "default_picture"]
            ],
        ];
    }

    /**
     * Provide data for the testLogin method
     * 
     * @return array
     */
    static public function loginProvider(): array
    {
        return [
            'Valid credentials' => [
                [
                    'email' => 'christopherdebray@gmail.com',
                    'password' => 'test',
                ],
                Response::HTTP_OK,
                []
            ],
            'Invalid credentials' => [
                [
                    'email' => 'nonexisting@gmail.com',
                    'password' => 'test',
                ],
                Response::HTTP_NOT_FOUND,
                ['email']
            ],
            'No password' => [
                [
                    'email' => 'christopherdebray@gmail.com',
                    'password' => '',
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ['password']
            ],
            'No email' => [
                [
                    'email' => '',
                    'password' => 'test',
                ],
                Response::HTTP_UNPROCESSABLE_ENTITY,
                ['email']
            ],
        ];
    }
}
