<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class AbstractTest extends TestCase
{
    use RefreshDatabase;

    public function generateAuthToken(string $email, string $password): string
    {
        $response = $this->postJson('/api/users/login', ['email' => $email, 'password' => $password]);

        $response->assertStatus(Response::HTTP_OK);

        return $response['data']['token'];
    }

    public function addAuthHeader(string $email, string $password): void
    {
        $userToken = $this->generateAuthToken($email, $password);

        $this->withHeader('Authorization', 'Bearer '.$userToken);
    }

    public function authUser(string $email, string $password): void
    {
        $this->addAuthHeader($email, $password);
    }

    public function authValidUser(): void
    {
        $this->authUser('christopherdebray@gmail.com', 'test');
    }
}
