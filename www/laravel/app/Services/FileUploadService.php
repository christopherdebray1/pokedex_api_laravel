<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileUploadService
{
    /**
     * Upload a file to the selected disk and path with a randomly generated names
     *
     * @param UploadedFile $file
     * @param string $uploadDirPath
     * @param string $disk
     *
     * @return string
     */
    public function uploadFile(UploadedFile $file, string $uploadDirPath, string $disk = 'public'): string
    {
        $fileName = Str::random(20) . '.' . $file->getClientOriginalExtension();

        Storage::disk($disk)->put($uploadDirPath, $file);

        return $uploadDirPath . '/' . $fileName;
    }
}
