<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Pokemon\Pokemon;
use OpenApi\Annotations as OA;
use App\Traits\Models\TimestampTrait;

/**
 * @OA\Schema(
 *      schema="User",
 *      description="User",
 *      @OA\Property(type="integer", property="id"),
 *      @OA\Property(type="string", example="Clark Heaney", property="name"),
 *      @OA\Property(type="string", example="gustave24@example.com", property="email"),
 *      @OA\Property(type="string", property="password"),
 *      @OA\Property(type="string", example="http://www.klein.org/,9xPZWKh8zJ", property="profile_picture"),
 * )
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use TimestampTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'profile_picture'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
    ];

    public function pokemons(): BelongsToMany
    {
        return $this->belongsToMany(Pokemon::class, 'user_pokemon');
    }
}
