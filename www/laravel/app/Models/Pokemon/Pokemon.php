<?php

namespace App\Models\Pokemon;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\User;

/**
 * @OA\Schema(
 *      schema="Pokemon",
 *      description="Pokemon",
 *      @OA\Property(type="integer", property="id"),
 *      @OA\Property(type="string", example="Charizard", property="name"),
 *      @OA\Property(type="string", example="#003", property="pokedex_id"),
 *      @OA\Property(type="string", example="http://www.klein.org/,9xPZWKh8zJ", property="picture_url"),
 * )
 */
class Pokemon extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];
    protected $table = 'pokemons';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'pokedex_id',
        'picture_url',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_pokemon');
    }
}
