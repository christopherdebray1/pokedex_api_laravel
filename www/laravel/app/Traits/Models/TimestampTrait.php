<?php

namespace App\Traits\Models;

trait TimestampTrait
{
  /**
   * @OA\Property(type="string",
   *     format="date-time",
   *     nullable=true,
   *     example="2023-11-22 09:56:37"
   * )
   * @var string|null
   */
    private string|null $createdAt;

  /**
   * @OA\Property(type="string",
   *     format="date-time",
   *     nullable=true,
   *     example="2023-11-22 09:56:37"
   * )
   * @var string|null
   */
    private string|null $updatedAt;
}
