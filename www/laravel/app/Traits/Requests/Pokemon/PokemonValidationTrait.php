<?php

namespace App\Traits\Requests\Pokemon;

trait PokemonValidationTrait
{
    /**
     * Provide ressource validation rules for pokemon creation
     *
     * @return array
     */
    protected function commonCreateRules(): array
    {
        return [
            "name" => "required|string|unique:pokemons",
            "pokedex_id" => "required|string|unique:pokemons|min:3|regex:/^[0-9]+$/",
            "picture_url" => "required|string|url:http,https",
        ];
    }
}
