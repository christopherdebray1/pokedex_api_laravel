<?php

namespace App\Http\Resources\Pokemon;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="PokemonResource",
 *      description="PokemonResource",
 *      @OA\Property(type="string", example="Charizard", property="name"),
 *      @OA\Property(type="string", example="#003", property="pokedex_id"),
 *      @OA\Property(type="string", example="http://www.klein.org/,9xPZWKh8zJ", property="picture_url"),
 * )
 */
class PokemonResource extends JsonResource
{
    public static $wrap = 'data';
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "name" => $this->name,
            "pokedex_id" => $this->pokedex_id,
            "picture_url" => $this->picture_url,
        ];
    }
}
