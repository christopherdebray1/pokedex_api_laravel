<?php

namespace App\Http\Resources\User;

use App\Traits\Models\TimestampTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="UserResource",
 *      description="UserResource",
 *      @OA\Property(type="string", example="Clark Heaney", property="name"),
 *      @OA\Property(type="string", example="gustave24@example.com", property="email"),
 * )
 */
class UserResource extends JsonResource
{
    use TimestampTrait;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
