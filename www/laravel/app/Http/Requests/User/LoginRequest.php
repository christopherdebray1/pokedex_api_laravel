<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *      request="LoginRequest",
 *      required=true,
 *      @OA\JsonContent(
 *          required={"email", "password"},
 *          @OA\Property(type="string", property="email", example="johndoe@gmail.com"),
 *          @OA\Property(type="string", property="password"),
 *      ),
 * )
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|max:100|email',
            'password' => 'required'
        ];
    }

    public function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'errors' => $validator->errors()
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
