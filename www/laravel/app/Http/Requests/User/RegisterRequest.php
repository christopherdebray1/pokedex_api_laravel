<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *      required=true,
 *      request="RegisterRequest",
 *      @OA\JsonContent(
 *          required={"name", "email", "password"},
 *          @OA\Property(type="string", property="name", example="John Doe"),
 *          @OA\Property(type="string", property="email", example="johndoe@gmail.com"),
 *          @OA\Property(type="string", property="password"),
 *          @OA\Property(type="string", property="profile_picture"),
 *          @OA\Property(type="string", property="default_picture", example="2"),
 *      ),
 * )
 */
class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:100',
            'email' => 'required|unique:users|max:100|email',
            'password' => 'required',
            'profile_picture' => 'required_without:default_picture|file|mimes:jpeg,png,jpg|max:10048',
            'default_picture' => 'required_without:profile_picture|integer',
        ];
    }

    public function failedValidation(Validator $validator): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'errors'    => $validator->errors()
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }
}
