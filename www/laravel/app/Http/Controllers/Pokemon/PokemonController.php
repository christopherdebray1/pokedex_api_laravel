<?php

namespace App\Http\Controllers\Pokemon;

use App\Http\Controllers\Controller;
use App\Http\Requests\Pokemon\PokemonCreateRequest;
use App\Http\Requests\Pokemon\PokemonTeamCreateRequest;
use App\Http\Resources\Pokemon\PokemonResource;
use App\Models\Pokemon\Pokemon;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use OpenAi\Annotations as OA;

class PokemonController extends Controller
{
    /**
     * Get all pokemon's
     *
     * @return JsonResponse
     *
     * @OA\Get(
     *      security={{ "bearer":{} }},
     *      path="/api/pokemons",
     *      summary="Get all pokemons",
     *      tags={"Pokemons"},
     *      @OA\Response(
     *          response="200",
     *          description="The pokemon",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PokemonResource"),
     *          ),
     *      ),
     *      @OA\Response(response="404", ref="#/components/responses/NotFound")
     * )
     */
    public function index(): JsonResponse
    {
        // return PokemonResource::collection(Pokemon::all());
        return response()->json([
            'success' => true,
            'data' => PokemonResource::collection(Pokemon::all()),
        ]);
    }

    /**
     * Get one pokemon by it's id
     *
     * @param Pokemon $pokemon
     *
     * @return JsonResponse
     *
     * @OA\Get(
     *      security={"bearer"},
     *      path="/api/pokemons/{id}",
     *      summary="Get one pokemon by it's id",
     *      tags={"Pokemons"},
     *      @OA\Parameter(ref="#/components/parameters/id"),
     *      @OA\Response(
     *          response="200",
     *          description="The pokemon",
     *          @OA\JsonContent(ref="#/components/schemas/PokemonResource"),
     *      ),
     *      @OA\Response(response="404", ref="#/components/responses/NotFound")
     * )
     */
    public function getOne(Pokemon $pokemon): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => (object)new PokemonResource($pokemon),
        ]);
    }

    /**
     * Get a pokemon team by it's user id
     *
     * @param User $user
     *
     * @return JsonResponse
     *
     * @OA\Get(
     *      security={"bearer"},
     *      path="/api/pokemons/users/team/{id}",
     *      summary="Get a pokemon team by it's user id",
     *      tags={"Pokemons"},
     *      @OA\Parameter(ref="#/components/parameters/id"),
     *      @OA\Response(
     *          response="200",
     *          description="The pokemon",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PokemonResource"),
     *          ),
     *      ),
     *      @OA\Response(response="404", ref="#/components/responses/NotFound"),
     * ),
     */
    public function getPokemonTeam(User $user): JsonResponse
    {
        return response()->json([
            'success' => true,
            'data' => (object)$user->pokemons,
        ]);
    }

    /**
     * @param PokemonCreateRequest $request
     * @return JsonResponse
     */
    public function create(PokemonCreateRequest $request): JsonResponse
    {
        $pokemon = Pokemon::create($request->validated());
        $pokemon->save();

        return response()->json([
            'success' => true,
            'data' => new PokemonResource($pokemon)
        ], Response::HTTP_CREATED);
    }

    /**
     * @param PokemonTeamCreateRequest $request
     * @param User $user
     * @return JsonResponse
     */
    public function createPokemonTeam(PokemonTeamCreateRequest $request, User $user): JsonResponse
    {
        if ($request->user()->id !== $user->id) {
            throw new HttpResponseException(response()->json([
                'success' => false,
                'errors' => [
                    'You can only change your team.',
                ]
            ], Response::HTTP_UNAUTHORIZED));
        }

        $payload = $request->validated();
        $pokemonTeamIds = [];
        foreach ($payload['pokemon_team'] as $pokemon) {
            if (!$pokemonEntity = Pokemon::where('pokedex_id', $pokemon['pokedex_id'])->first()) {
                $pokemonEntity = Pokemon::create($pokemon);
            }

            $pokemonTeamIds[] = $pokemonEntity->id;
        }

        $user->pokemons()->sync($pokemonTeamIds);
        $user->save();

        return response()->json([
            $payload,
        ]);
    }
}
