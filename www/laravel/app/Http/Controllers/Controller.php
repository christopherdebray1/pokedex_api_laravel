<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="Pokedex api", version="1"),
 * @OA\Server(
 *    url="http://pokedex_api.local/",
 *    description="A pokedex api",
 * ),
 *
 * @OA\Parameter(
 *      name="id",
 *      in="path",
 *      description="ID of the ressource",
 *      required=true,
 *      @OA\Schema(type="integer"),
 * ),
 *
 * @OA\Response(
 *      response="NotFound",
 *      description="This ressource doesn't exist",
 *      @OA\JsonContent(
 *          @OA\Property(
 *              property="success",
 *              type="boolean",
 *              example=false
 *          ),
 *      ),
 * ),
 *
 * @OA\SecurityScheme(bearerFormat="JWT", type="apiKey", securityScheme="bearer", in="header", name="Authorization"),
 */
class Controller extends BaseController
{
    use AuthorizesRequests;
    use ValidatesRequests;
}
