<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User\UserResource;
use App\Services\FileUploadService;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

class UserController extends Controller
{
    /**
     * Register the user into our database
     *
     * @param RegisterRequest $request
     * @param FileUploadService $fileUploadService
     *
     * @return JsonResponse
     *
     * @throws HttpResponseException
     *
     * @OA\Post(
     *      path="/api/users/register",
     *      summary="Register a user",
     *      tags={"Users"},
     *      @OA\RequestBody(ref="#/components/requestBodies/RegisterRequest"),
     *      @OA\Response(
     *          response="201",
     *          description="The created user",
     *          @OA\Property(ref="#/components/schemas/UserResource"),
     *      ),
     *      @OA\Response(
     *          response="422",
     *          description="The email has already been taken.",
     *          @OA\JsonContent(
     *              description="Already taken email error",
     *              @OA\Property(type="string", property="errors", example="The email has already been taken.")
     *          ),
     *      ),
     * )
     */
    public function register(RegisterRequest $request, FileUploadService $fileUploadService): JsonResponse
    {
        $validatedRequest = $request->validated();
        if (empty($validatedRequest['profile_picture']) && $validatedRequest['default_picture']) {
            $fileName = "user/profile_pictures/default_profile_" . $validatedRequest['default_picture'] . ".jpeg";
            if (Storage::disk('public_predefined')->get($fileName)) {
                $validatedRequest['profile_picture'] = $fileName;
            }
        } else {
            $user['profile_picture'] = $fileUploadService->uploadFile(
                $validatedRequest['profile_picture'],
                'uploads/user/profile_pictures'
            );
        }

        $user = User::create($validatedRequest);
        $user->save();

        return response()->json([
            'success' => true,
            'data' => (object)[
                'user' => new UserResource($user),
            ]
        ], Response::HTTP_CREATED);
    }

    /**
     * Login the user by generating and returning an auth token
     *
     * @param LoginRequest $request
     *
     * @return JsonResponse
     *
     * @throws HttpResponseException
     *
     * @OA\Post(
     *     path="/api/users/login",
     *     summary="Login the user and return a jwt",
     *     tags={"Users"},
     *     @OA\RequestBody(ref="#/components/requestBodies/LoginRequest"),
     *     @OA\Response(
     *          response="200",
     *          description="The user token",
     *          @OA\JsonContent(
     *              description="User connection token",
     *              @OA\Property(property="token", type="string", description="The auth token"),
     *          ),
     *      ),
     *     @OA\Response(
     *          response="404",
     *          description="Wrong credentials.",
     *          @OA\JsonContent(
     *              description="Wrong credentials.",
     *              @OA\Property(
     *                  property="errors",
     *                  type="string",
     *                  description="The auth token",
     *                  example="Wrong credentials."
     *              ),
     *          ),
     *      ),
     * )
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $payload = $request->validated();
        /** @var User $user */
        if (!$user = User::where('email', $payload['email'])->first()) {
            throw new HttpResponseException(response()->json([
                'success' => false,
                'errors' => (object)[
                    "email" => [
                        'Wrong credentials.'
                    ]
                ]
            ], Response::HTTP_NOT_FOUND));
        }

        if (!Hash::check($payload['password'], $user->password)) {
            throw new HttpResponseException(response()->json([
                'success' => false,
                'errors' => (object)[
                    "email" => [
                        'Wrong credentials.'
                    ]
                ]
            ], Response::HTTP_NOT_FOUND));
        }

        return response()->json([
            'success' => true,
            'data' => (object)[
                'token' => $user->createToken('auth_token')->plainTextToken,
            ]
        ]);
    }

    /**
     * Disconnect the user by deleting his token
     *
     * @param Request $request
     *
     * @return void
     */
    public function logout(Request $request): void
    {
        $request->user()->currentAccessToken()->delete();
    }
}
