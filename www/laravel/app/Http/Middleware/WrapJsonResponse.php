<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class WrapJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\JsonResponse)  $next
     */
    public function handle(Request $request, Closure $next): JsonResponse
    {
        $response = $next($request);

        if (!$this->isException($response)) {
            $content = $response->getData(true);
            $wrappedData = ['data' => $content];
            $response->setData($wrappedData);
        }

        return $response;
    }

    protected function isException(JsonResponse|Response $response): bool
    {
        return $response->exception instanceof HttpResponseException;
    }
}
