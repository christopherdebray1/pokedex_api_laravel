# Executables
DOCKER           = docker
DOCKER_EXEC    = $(DOCKER) exec
DOCKER_PHP_BASH = $(DOCKER_EXEC) -it api-web-1 sh

# Variables
DOCKER_PROJECT_PATH= laravel
LOCAL_PROJECT_PATH= www/laravel
PHP_CS_PARAMS= -s --standard=$(PHP_STANDARD) app
ENV_TESTING_PARAMS= --env=testing
PHP_STANDARD= PSR12

# Container names
DOCKER_DB_TEST= api-database-test-1

# Colors
NOCOLOR=\033[0m
RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
BLUE=\033[0;34m
CYAN=\033[0;36m

# Vendors
PHP_CS_FIXER  = php vendor/bin/phpcs
PHP_CBF_FIXER  = php vendor/bin/phpcbf

.PHONY: php-cs-fixer php-cbf-fixer prepare-db-testing start-functional-test open-api-generate activate-virtual-host compose-up

## DOCKER BUILD
compose-up:
	@printf "$(CYAN)Create a mysql user for the testing database$(NOCOLOR) \n"
	docker compose up --build
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan migrate"
	@printf "$(CYAN)Enables the virtual host allowing to use pokedex_api.local as local url for the api $(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "a2ensite pokedex_api.local && service apache2 reload"

### In local the virtual-host is deactivated when you delete the container. I don't think it is deleted on container stop but in case it is you can launch this command
virtual-host-activate:
	@printf "$(CYAN)Enables the virtual host allowing to use pokedex_api.local as local url for the api $(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "a2ensite pokedex_api.local && service apache2 reload"
# $(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan migrate --seed"

## CODE CLEANER
php-cs-fixer: ## run php cs-fixer
	@printf "$(CYAN)Run php cs fixer without fixing the errors$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && $(PHP_CS_FIXER) $(PHP_CS_PARAMS)"

php-cbf-fixer: ## run php cs-fixer
	@printf "$(CYAN)Run php cs fixer and fix the errors$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && $(PHP_CBF_FIXER) $(PHP_CS_PARAMS)"

## TESTING
db-testing-setup: ## Setup testing database container
	@printf "$(CYAN)Create a mysql user for the testing database$(NOCOLOR) \n"
	docker exec --env-file $(LOCAL_PROJECT_PATH)/.env.testing $(DOCKER_DB_TEST) mysql -u root -e "CREATE USER IF NOT EXISTS 'testing_user'@'%' IDENTIFIED BY 'test'; GRANT ALL PRIVILEGES ON *.* TO 'testing_user'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;" || true

db-testing-prepare: ## Prepare the testing database by creating it with all the migrations and seeders
	@printf "$(CYAN)Drop pokedex database if exist$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan db:drop $(ENV_TESTING_PARAMS) 2>/dev/null"
	@printf "$(CYAN)Create pokedex database$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan db:create $(ENV_TESTING_PARAMS)"
	@printf "$(CYAN)Execute migrations and seeders$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan migrate --seed $(ENV_TESTING_PARAMS)"

start-functional-test:
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && php artisan test $(ENV_TESTING_PARAMS)"

## DOCUMENTATION
open-api-generate:
	@printf "$(CYAN)Generate the yaml from the open api docs$(NOCOLOR) \n"
	$(DOCKER_PHP_BASH) -c "cd $(DOCKER_PROJECT_PATH) && ./vendor/bin/openapi --format json --output ./public/swagger/swagger.json app"